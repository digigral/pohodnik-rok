<?php
/**
 * Template Name: Quercus Landing
 *
 */
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Vino ima svoj Žar</title>

    <!-- FavIcons -->
    <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="icon" type="image/x-icon">
    <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">

    <!-- Apple Icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/img/icons/icon-144x144.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/img/icons/icon-114x114.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/img/icons/icon-72x72.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/img/icons/icon-160x160.png">

    <?php wp_head(); ?>

    <meta property="og:url"           content="https://klet-brda.si/sl/nagradna-igra-quercus/" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="Nagradna igra: Vino ima svoj Žar"/>
    <meta property="og:description"   content="Sodeluj v nagradni igri in se poteguj za žar Weber ali priročno pakirano vino Quercus to go." />
    <meta property="og:image"         content="<?php echo get_template_directory_uri() . '/quercus-assets/images/fb-meta-quercus.jpg'; ?>" />

    <!-- all external stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600&amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="/wp-content/themes/brda/quercus-assets/styles/main.css">

	
	<!-- Google Analytics without Cookie -->
		<script type="text/javascript" src="/wp-content/themes/brda/ga.php?v=<?php echo time(); ?>"></script>
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-39893141-53', {'storage': 'none', 'clientId': client_id});
			ga('send', 'pageview', {'anonymizeIp': true});
		</script>
		<!-- End of Google Analytics -->
	
  </head>
  <body>
    
    <!--[if lt IE 8]>
        <p>You are using an <strong>outdated</strong> browser. Please upgrade your browser to improve your experience.</p>
    <![endif]-->

    <header class="header-main">
      <div class="container" style="position: relative;">
        <div id="hamb-icon" class="d-inline-block d-lg-none">
          <span></span>
          <span></span>
          <span></span>
        </div>
        <div class="row">
          <div class="col-lg-4 col-9">
            <div class="d-flex align-items-center justify-content-start">
                <img class="img-fluid header-logo" src="/wp-content/themes/brda/quercus-assets/images/klet-brda-logo.png" alt="Klet Brda Logotip">
                <img class="img-fluid header-quercus-logo d-inline-block d-none" src="/wp-content/themes/brda/quercus-assets/images/quercus-to-go.svg" alt="Quercus To Go Logotip">
            </div>
          </div>
          <div class="col-lg-8 col-12">
            <nav class="header-nav">
              <ul>
                <!-- <li class="d-none d-lg-inline-block"><img class="img-fluid header-quercus-logo" src="dist/images/quercus-to-go.svg" alt="Quercus To Go Logotip"></li> -->
                <li class="menu-item"><a href="<?php the_permalink(7694); ?>">Nagradna igra</a></li>
                <li class="menu-item"><a href="https://klet-brda.si/sl/kategorija/quercus/quercus-to-go/">Nakup</a></li>
                <li class="menu-item"><a href="https://klet-brda.si/wp-content/uploads/2019/04/PRAVILA-NAGRADNE-IGRE-2019p.pdf" target="_blank">Pravila in pogoji</a></li>
                <li class="social-header">
                  <div class="d-flex align-items-center">
                    <span>deli s prijatelji</span>
                    <!-- Load Facebook SDK for JavaScript -->
                    <div id="fb-root"></div>
                    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/sl_SI/sdk.js#xfbml=1&version=v3.2&appId=1671310966328837&autoLogAppEvents=1"></script>
                    <div data-href="https://klet-brda.si/sl/nagradna-igra-quercus/" data-layout="button_count" data-size="small"><a class="fb" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fklet-brda.si%2Fsl%2Fnagradna-igra-quercus%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore"><i class="fab fa-facebook-f"></i></a></div>
                    <a class="insta" href="https://www.instagram.com/kletbrda/" target="_blank"><i class="fab fa-instagram"></i></a>
                  </div> 
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </header>

    <main class="content-main">

      <section id="section-banner-top">
        <div class="container">
            <img class="img-fluid img-top" src="/wp-content/themes/brda/quercus-assets/images/banner-top-napis.svg" alt="Quercus - Vino ima svoj žar">
            <img class="img-fluid img-bottom" src="/wp-content/themes/brda/quercus-assets/images/banner-top-nagrade.png" alt="Quercus nagradna igra">
        </div>
      </section>

      <section id="section-sodeluj">
        <div class="up-bg">
          <div class="scroll-icon js--scroll-icon" data-target="#section-sodeluj"><span class="icon"></span></div>
        </div>
        <div class="down-bg"></div>

        <div class="container">
          <div class="row">
              <div class="custom-col col-lg-3 d-none d-lg-block">
                  <img class="img-fluid img1" src="/wp-content/themes/brda/quercus-assets/images/sodeluj-hrenovka.svg" alt="Sodeluj v nagradni igri slika 1">
              </div>
              <div class="custom-col col-lg-5">
                  <div class="text-wrapper">
                      <p class="white-text">Sodeluj v <br><span>nagradni igri</span><br> in osvoji žar.</p>
                      <p><a href="<?php the_permalink(7694); ?>" class="btn btn-primary">Sodeluj</a></p>
                  </div>
              </div>

              <div class="custom-col col-lg-4">
                <img class="img-fluid img2" src="/wp-content/themes/brda/quercus-assets/images/zar-steak.svg" alt="Žar steak">
                <img class="img-fluid img3" src="/wp-content/themes/brda/quercus-assets/images/sodeluj-zar-icon.svg" alt="Žar ikona">
              </div>
          </div>
        </div>

      </section>

      <section id="section-nagrade">

          <div class="up-bg">
              <div class="scroll-icon js--scroll-icon" data-target="#nagrade-title-scroll"><span class="icon"></span></div>
          </div>

          <div class="container">
              <div class="row">
                <div class="col-lg-4">
                    <img class="img-fluid img1 d-none d-lg-block" src="/wp-content/themes/brda/quercus-assets/images/meso-zar.svg" alt="">
                    <img class="img-fluid img2 d-none d-lg-block" src="/wp-content/themes/brda/quercus-assets/images/weber-zar.png" alt="">
                </div>
                <div class="col-lg-8">
                  <div class="nagrade-content-wrapper">
                      <h2 class="nagrade-title" id="nagrade-title-scroll">Nagrade</h2>
                      <ul class="nagrade-list">
                        <li>
                          <span class="whole-text">
                            <span class="number">1</span>&nbsp; 
                            <span class="x">x</span>&nbsp; 
                            <span class="red">žar</span>&nbsp; 
                            <span class="black">Weber</span>
                          </span> 
                          <img class="img-fluid d-none d-lg-block" src="/wp-content/themes/brda/quercus-assets/images/nagrade-list-icon.svg" alt=""></li>
                        <li>
                          <span class="whole-text">
                            <span class="number">15</span>&nbsp;
                            <span class="x">x</span>&nbsp;
                            <span class="black">3 Litrsko</span>&nbsp;
                            <span class="red">pakiranje</span>&nbsp;
                            <br>
                            <span class="bottom-red">White + Red selection = 3l x 2</span>
                          </span> 
                          <img class="img-fluid d-none d-lg-block" src="/wp-content/themes/brda/quercus-assets/images/nagrade-list-icon.svg" alt=""></li>
                      </ul>
                      <img class="img-fluid quercus-img" src="/wp-content/themes/brda/quercus-assets/images/quercus-to-go.svg" alt="">
                  </div>
                </div>
              </div>
              <img class="img-fluid img-fire-bottom" src="/wp-content/themes/brda/quercus-assets/images/ogenj-ikona.svg" alt="">
          </div>

      </section>

      <section id="section-nagradna-igra">
          <div class="up-bg">
            <div class="scroll-icon js--scroll-icon" data-target="#nagradna-datum-scroll"><span class="icon"></span></div>
          </div>
          <div class="down-bg"></div>
          <div class="container">
            <div class="row">
              <div class="col-12">
                <div class="text-wrapper" id="nagradna-datum-scroll">
                    <p><a href="<?php the_permalink(7694); ?>" class="btn btn-primary">Sodeluj</a></p>
                    <h3 class="title-white">Nagradna igra poteka od<br><span class="bottom">8. 4. 2019 do 15. 5. 2019.</span></h3>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section id="section-photos">
            <div class="custom-flex-wrapper">
                <div class="col-custom first">
                    <img class="img-fluid first" src="/wp-content/themes/brda/quercus-assets/images/news-01.jpg" alt="">
                    <div class="text-wrapper">
                      <h3>Kampiranje</h3>
                      <p>Na kampiranje z vrhunskimi vini Quercus TO GO - priročno pakiranje, ki ohranja primerno temperaturo vina.</p>
                    </div>
                </div>
                <div class="col-custom second">
                    <img class="img-fluid second" src="/wp-content/themes/brda/quercus-assets/images/news-02.jpg" alt="">
                    <div class="text-wrapper">
                      <h3>Počitnice</h3>
                      <p>Na daljše potovanje s Quercus TO GO - priročno pakiranje ohranja vino sveže tudi do 4 tedne po odprtju.</p>
                    </div>
                </div>
                <div class="col-custom third">
                    <img class="img-fluid third" src="/wp-content/themes/brda/quercus-assets/images/news-03.jpg" alt="">
                    <div class="text-wrapper">
                      <h3>Jadranje</h3>
                      <p>Na jadranje z vrhunskimi vini Quercus TO GO - priročno pakiranje, ki je okolju prijaznejše od steklenic.</p>
                    </div>
                </div>
            </div>
        </section>

        <section id="section-quercus-bottom">
          <div class="container">
            <div class="text-wrapper">
              <div class="title-logo">
                  <img class="img-fluid quercus-logo-bottom" src="/wp-content/themes/brda/quercus-assets/images/quercus-logo-bottom.svg" alt="Quercus Logotip Spodaj">
              </div>
              <p>Quercus Selection je nadgradnja družine vrhunskih vin Quercus v okolju prijazni in priročni embalaži. Tako lahko uživate v tej omejeni seriji vin tudi na počitnicah.</p>
              <img class="img-fluid quercus-to-go-card" src="/wp-content/themes/brda/quercus-assets/images/quercus-to-go-card.svg" alt="Quercus To Go">
              <div class="scroll-icon top" id="js-scroll-top"><span class="icon"></span></div>
            </div>
          </div>
        </section>

    </main>


    <!-- all external scripts -->
    <script src="/wp-content/themes/brda/quercus-assets/scripts/vendor.js"></script>
    <script src="/wp-content/themes/brda/quercus-assets/scripts/main.js"></script>

    <?php get_footer(); ?>

